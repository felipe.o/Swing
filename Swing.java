import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class Swing{
	private JFrame mainFrame;
	private JLabel headerLabel;
	private JLabel statusLabel;
	private JPanel controlPanel;

	public Swing(){
		preparaGUI(); 
	}

	public static void main(String[] args){
		Swing exemplo = new Swing();
		exemplo.mostrarExemplo();
	}

	private void preparaGUI(){
		mainFrame = new JFrame("Exemplo java SWING");
		mainFrame.setSize(400, 400);
		mainFrame.setLayout(new GridLayout(3,1));

		headerLabel = new JLabel("", JLabel.CENTER);
		statusLabel = new JLabel("", JLabel.CENTER);


		statusLabel.setSize(350, 100);
		mainFrame.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent windowEvent){
				System.exit(0);
			}
		});
		controlPanel = new JPanel();
		controlPanel.setLayout(new FlowLayout());

		mainFrame.add(headerLabel);
		mainFrame.add(controlPanel);
		mainFrame.add(statusLabel);
		mainFrame.setVisible(true);
	}

	private void mostrarExemplo(){
		headerLabel.setText("Escolha quem sabe programar");

		JButton okButton = new JButton("Felipe");
		JButton submitButton = new JButton("Guilherme");
		JButton cancelButton = new JButton("Potter");

		okButton.setActionCommand("Felipe");
		submitButton.setActionCommand("Guilherme");
		cancelButton.setActionCommand("Potter");

		okButton.addActionListener(new ButtonClickListener());
		submitButton.addActionListener(new ButtonClickListener());
		cancelButton.addActionListener(new ButtonClickListener());

		controlPanel.add(okButton);
		controlPanel.add(submitButton);
		controlPanel.add(cancelButton);

		mainFrame.setVisible(true);
	}

	private class ButtonClickListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			String command = e.getActionCommand();
			if(command.equals("Felipe"))
				statusLabel.setText("Sabe programar ");
			else if(command.equals("Guilherme"))
				statusLabel.setText("Sabe programar");
			else
				statusLabel.setText("Não sabe programar");
		}
	}
}